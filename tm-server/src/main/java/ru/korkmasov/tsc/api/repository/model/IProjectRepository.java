package ru.korkmasov.tsc.api.repository.model;

import ru.korkmasov.tsc.api.IRepository;
import ru.korkmasov.tsc.model.ProjectGraph;

import java.util.List;

public interface IProjectRepository extends IRepository<ProjectGraph> {

    void update(final ProjectGraph project);

    ProjectGraph findByIdUserId(final String userId, final String id);

    void clearByUserId(final String userId);

    void removeByIdUserId(final String userId, final String id);

    List<ProjectGraph> findAllByUserId(final String userId);

    ProjectGraph findByName(final String userId, final String name);

    ProjectGraph findByIndex(final String userId, final int index);

    void removeByName(final String userId, final String name);

    void removeByIndex(final String userId, final int index);

}
