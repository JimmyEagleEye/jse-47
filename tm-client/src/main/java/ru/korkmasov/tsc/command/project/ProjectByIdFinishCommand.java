package ru.korkmasov.tsc.command.project;

import ru.korkmasov.tsc.exception.entity.ProjectNotFoundException;
//import ru.korkmasov.tsc.model.Project;
import ru.korkmasov.tsc.model.User;
import ru.korkmasov.tsc.util.TerminalUtil;
import org.jetbrains.annotations.Nullable;
import ru.korkmasov.tsc.command.project.AbstractProjectCommand;
import ru.korkmasov.tsc.endpoint.Project;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ProjectByIdFinishCommand extends AbstractProjectCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "project-finish-by-id";
    }

    @Override
    public @NotNull String description() {
        return "Finish project by id";
    }

    @Override
    public void execute() {
        System.out.println("[FINISH PROJECT]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        //@Nullable final Project project = serviceLocator.getProjectEndpoint().finishProjectById(getSession(), id);
        //if (project == null) throw new ProjectNotFoundException();
    }

}

