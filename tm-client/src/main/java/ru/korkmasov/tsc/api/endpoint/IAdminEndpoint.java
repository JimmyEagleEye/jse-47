package ru.korkmasov.tsc.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.korkmasov.tsc.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IAdminEndpoint {

    @WebMethod
    void loadBackup(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    );

    @WebMethod
    void loadJson(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    );

    @WebMethod
    void saveBackup(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    );

    @WebMethod
    void saveJson(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    );

}
