package ru.korkmasov.tsc.bootstrap;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.korkmasov.tsc.api.repository.ICommandRepository;
import ru.korkmasov.tsc.api.service.ICommandService;
import ru.korkmasov.tsc.api.service.ILoggerService;
import ru.korkmasov.tsc.api.service.IPropertyService;
import ru.korkmasov.tsc.api.service.ServiceLocator;
import ru.korkmasov.tsc.command.AbstractCommand;
import ru.korkmasov.tsc.component.FileScanner;
import ru.korkmasov.tsc.constant.TerminalConst;
import ru.korkmasov.tsc.endpoint.*;
import ru.korkmasov.tsc.exception.system.UnknownCommandException;
import ru.korkmasov.tsc.repository.CommandRepository;
import ru.korkmasov.tsc.service.CommandService;
import ru.korkmasov.tsc.service.LoggerService;
import ru.korkmasov.tsc.service.PropertyService;
import ru.korkmasov.tsc.util.TerminalUtil;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
//import sun.rmi.transport.Endpoint;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

import static ru.korkmasov.tsc.util.SystemUtil.getPID;
import static ru.korkmasov.tsc.util.TerminalUtil.displayWait;
import static ru.korkmasov.tsc.util.TerminalUtil.displayWelcome;

@Getter
public class Bootstrap implements ServiceLocator {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final AdminEndpointService adminEndpointService = new AdminEndpointService();

    @NotNull
    private final AdminEndpoint adminEndpoint = adminEndpointService.getAdminEndpointPort();

    @NotNull
    private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();

    @NotNull
    private final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    private final TaskEndpointService taskEndpointService = new TaskEndpointService();

    @NotNull
    private final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();

    @NotNull
    private final DataEndpointService dataEndpointService = new DataEndpointService();

    @NotNull
    private final DataEndpoint dataEndpoint = dataEndpointService.getDataEndpointPort();

    @NotNull
    private final ILoggerService logService = new LoggerService();

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    @Nullable
    private static Session session;

    @Nullable
    public Session getSession() {
        return session;
    }

    @Override
    public void setSession(Session session) {
        Bootstrap.session = session;
    }

    public void start(String... args) {
        displayWelcome();
        initCommands();
        if (runArgs(args)) System.exit(0);
        process();
    }

    public void initApplication() {
        initPID();
    }


    @SneakyThrows
    private void initCommands() {
        @NotNull final Reflections reflections = new Reflections("ru.korkmasov.tsc.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(ru.korkmasov.tsc.command.AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            final boolean isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            registry(clazz.newInstance());
        }
    }


    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();

    }

    private boolean runArgs(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final AbstractCommand command = commandService.getCommandByArg(args[0]);
        if (command == null) throw new UnknownCommandException(args[0]);
        command.execute();
        return true;
    }

    public void runCommand(@Nullable final String command) {
        if (command == null || command.isEmpty()) return;
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new UnknownCommandException(command);
        abstractCommand.execute();
    }

    private void registry(@Nullable AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void process() {
        logService.debug("Test environment.");
        @Nullable String command = "";
        fileScanner.init();
        while (!TerminalConst.CMD_EXIT.equals(command)) {
            try {
                displayWait();
                command = TerminalUtil.nextLine();
                logService.command(command);
                runCommand(command);
                logService.info("Completed");
            } catch (Exception e) {
                logService.error(e);
            }
        }
    }
}