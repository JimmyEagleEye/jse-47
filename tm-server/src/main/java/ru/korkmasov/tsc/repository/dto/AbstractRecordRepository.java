package ru.korkmasov.tsc.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.korkmasov.tsc.dto.AbstractRecord;

import javax.persistence.EntityManager;
import java.util.Collection;
import javax.persistence.TypedQuery;
import java.util.List;

public abstract class AbstractRecordRepository<E extends AbstractRecord> {

    @NotNull
    protected final EntityManager entityManager;

    protected AbstractRecordRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void add(@Nullable final E entity) {
        if (entity == null) return;
        entityManager.persist(entity);
    }

    public void update(@Nullable final E entity) {
        if (entity == null) return;
        entityManager.merge(entity);
    }

    public void addAll(@Nullable final Collection<E> collection) {
        if (collection == null) return;
        for (E item : collection) {
            add(item);
        }
    }

    protected E getSingleResult(@NotNull final TypedQuery<E> query) {
        @NotNull final List<E> list = query.getResultList();
        if (list.isEmpty()) return null;
        return list.get(0);
    }

}