package ru.korkmasov.tsc.api.entity;

import org.jetbrains.annotations.Nullable;

import ru.korkmasov.tsc.enumerated.Status;

public interface IHasStatus {

    @Nullable
    Status getStatus();

    void setStatus(@Nullable Status status);

}
