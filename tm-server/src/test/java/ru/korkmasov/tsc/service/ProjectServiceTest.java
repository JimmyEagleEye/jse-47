package ru.korkmasov.tsc.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.korkmasov.tsc.TestConnectionService;
import ru.korkmasov.tsc.dto.ProjectRecord;
import ru.korkmasov.tsc.dto.UserRecord;
import ru.korkmasov.tsc.exception.empty.EmptyIdException;
import ru.korkmasov.tsc.exception.empty.EmptyIndexException;
import ru.korkmasov.tsc.exception.empty.EmptyNameException;
import ru.korkmasov.tsc.exception.system.IndexIncorrectException;
import ru.korkmasov.tsc.marker.DBCategory;
import ru.korkmasov.tsc.service.dto.ProjectRecordService;
import ru.korkmasov.tsc.service.dto.UserRecordService;


import java.util.List;

public class ProjectServiceTest {

    @Nullable
    private static ProjectRecordService projectService;

    @Nullable
    private static UserRecord user;

    @Nullable
    private ProjectRecord project;

    @BeforeClass
    public static void beforeClass() {
        projectService = new ProjectRecordService(new TestConnectionService());
        @NotNull final UserRecordService userService =
                new UserRecordService(new TestConnectionService(), new PropertyService());
        userService.add("user_project", "user");
        user = userService.findByLogin("user_project");
    }

    @Before
    public void before() {
        project = projectService.add(user.getId(), new ProjectRecord("Project"));
    }

    @Test
    @Category(DBCategory.class)
    public void add() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        Assert.assertNotNull(project.getName());
        Assert.assertEquals("Project", project.getName());

        @NotNull final ProjectRecord projectById = projectService.findById(project.getId());
        Assert.assertNotNull(projectById);
        Assert.assertEquals(project.getId(), projectById.getId());
    }

    @Test
    @Category(DBCategory.class)
    public void findAll() {
        @NotNull final List<ProjectRecord> projects = projectService.findAll();
        Assert.assertTrue(projects.size() > 0);
    }

    @Test
    @Category(DBCategory.class)
    public void findAllByUserId() {
        @NotNull final List<ProjectRecord> projects = projectService.findAll(user.getId());
        Assert.assertTrue(projects.size() > 0);
    }

    @Test
    @Category(DBCategory.class)
    public void findAllByUserIdIncorrect() {
        @NotNull final List<ProjectRecord> projects = projectService.findAll("test");
        Assert.assertNotEquals(1, projects.size());
    }

    @Test
    @Category(DBCategory.class)
    public void findById() {
        @NotNull final ProjectRecord project = projectService.findById(user.getId(), this.project.getId());
        Assert.assertNotNull(project);
    }

    @Test
    @Category(DBCategory.class)
    public void findByIdIncorrect() {
        @NotNull final ProjectRecord project = projectService.findById(user.getId(), "34");
        Assert.assertNull(project);
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyIdException.class)
    public void findByIdNull() {
        @NotNull final ProjectRecord project = projectService.findById(user.getId(), null);
        Assert.assertNull(project);
    }

    @Test
    @Category(DBCategory.class)
    public void findByIdIncorrectUser() {
        @NotNull final ProjectRecord project = projectService.findById("test", this.project.getId());
        Assert.assertNull(project);
    }

    @Test
    @Category(DBCategory.class)
    public void remove() {
        projectService.removeById(project.getId());
        Assert.assertNull(projectService.findById(project.getId()));
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyIdException.class)
    public void testRemoveNull() {
        projectService.removeById(null);
    }

    @Test
    @Category(DBCategory.class)
    public void findByName() {
        @NotNull final ProjectRecord project = projectService.findByName(user.getId(), "Project");
        Assert.assertNotNull(project);
    }

    @Test
    @Category(DBCategory.class)
    public void findByNameIncorrect() {
        @NotNull final ProjectRecord project = projectService.findByName(user.getId(), "34");
        Assert.assertNull(project);
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyNameException.class)
    public void findByNameNull() {
        @NotNull final ProjectRecord project = projectService.findByName(user.getId(), null);
        Assert.assertNull(project);
    }

    @Test
    @Category(DBCategory.class)
    public void findByNameIncorrectUser() {
        @NotNull final ProjectRecord project = projectService.findByName("test", this.project.getName());
        Assert.assertNull(project);
    }

    @Test
    @Category(DBCategory.class)
    public void findByIndex() {
        @NotNull final ProjectRecord project = projectService.findByIndex(user.getId(), 1);
        Assert.assertNotNull(project);
    }

    @Category(DBCategory.class)
    @Test(expected = IndexIncorrectException.class)
    public void findByIndexIncorrect() {
        @NotNull final ProjectRecord project = projectService.findByIndex(user.getId(), 34);
        Assert.assertNull(project);
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyIndexException.class)
    public void findByIndexNull() {
        @NotNull final ProjectRecord project = projectService.findByIndex(user.getId(), null);
        Assert.assertNull(project);
    }

    @Category(DBCategory.class)
    @Test(expected = IndexIncorrectException.class)
    public void findByIndexIncorrectUser() {
        @NotNull final ProjectRecord project = projectService.findByIndex("test", 0);
        Assert.assertNull(project);
    }

    @Test
    @Category(DBCategory.class)
    public void removeById() {
        projectService.removeById(user.getId(), project.getId());
        Assert.assertNull(projectService.findById(project.getId()));
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyIdException.class)
    public void removeByIdNull() {
        projectService.removeById(user.getId(), null);
    }

    @Category(DBCategory.class)
    @Test(expected = IndexIncorrectException.class)
    public void removeByIndexIncorrect() {
        projectService.removeByIndex(user.getId(), 34);
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyIndexException.class)
    public void removeByIndexNull() {
        projectService.removeByIndex(user.getId(), null);
    }

    @Category(DBCategory.class)
    @Test(expected = IndexIncorrectException.class)
    public void removeByIndexIncorrectUser() {
        projectService.removeByIndex("test", 0);
    }


    @Category(DBCategory.class)
    @Test(expected = EmptyNameException.class)
    public void removeByNameNull() {
        projectService.removeByName(user.getId(), null);
    }


}