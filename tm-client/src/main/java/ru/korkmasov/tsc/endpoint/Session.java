package ru.korkmasov.tsc.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "session", propOrder = {
        "timestamp",
        "userId",
        "signature"
})
public class Session
        extends AbstractEntity {

    protected Long timestamp;
    protected String userId;
    protected String signature;

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long value) {
        this.timestamp = value;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String value) {
        this.userId = value;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String value) {
        this.signature = value;
    }

}
