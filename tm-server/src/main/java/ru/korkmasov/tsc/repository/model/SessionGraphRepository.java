package ru.korkmasov.tsc.repository.model;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.korkmasov.tsc.api.repository.model.ISessionRepository;
import ru.korkmasov.tsc.model.SessionGraph;

import javax.persistence.EntityManager;
import java.util.List;

public final class SessionGraphRepository extends AbstractGraphRepository<SessionGraph> implements ISessionRepository {

    public SessionGraphRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    public SessionGraph getReference(@NotNull final String id) {
        return entityManager.getReference(SessionGraph.class, id);
    }

    @Override
    public List<SessionGraph> findAllByUserId(String userId) {
        return entityManager
                .createQuery("SELECT e FROM Session e WHERE e.user.id = :userId", SessionGraph.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public void removeByUserId(String userId) {
        entityManager
                .createQuery("DELETE FROM Session e WHERE e.user.id = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    @NotNull
    public List<SessionGraph> findAll() {
        return entityManager.createQuery("SELECT e FROM Session e", SessionGraph.class).getResultList();
    }

    public SessionGraph findById(@Nullable final String id) {
        return entityManager.find(SessionGraph.class, id);
    }

    public void clear() {
        entityManager
                .createQuery("DELETE FROM Session e")
                .executeUpdate();
    }

    public void removeById(@Nullable final String id) {
        SessionGraph reference = entityManager.getReference(SessionGraph.class, id);
        entityManager.remove(reference);
    }
}