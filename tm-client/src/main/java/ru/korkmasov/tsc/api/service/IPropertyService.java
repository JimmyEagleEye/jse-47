package ru.korkmasov.tsc.api.service;

import org.jetbrains.annotations.NotNull;
import ru.korkmasov.tsc.api.other.ISaltSettings;

public interface IPropertyService extends ISaltSettings {

    @NotNull String getApplicationVersion();

    int getBackupInterval();

    int getScannerInterval();

    @NotNull String getServerHost();

    int getServerPort();

    int getSessionCycle();

    @NotNull String getSessionSalt();

    public String getJdbcUser();

    public String getJdbcPassword();

    public String getJdbcUrl();

    public String getJdbcDriver();

    public String getHibernateDialect();

    public String getHibernateBM2DDLAuto();

    public String getHibernateShowSql();



}
