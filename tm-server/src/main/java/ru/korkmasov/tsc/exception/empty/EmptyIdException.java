package ru.korkmasov.tsc.exception.empty;


import ru.korkmasov.tsc.exception.AbstractException;

public class EmptyIdException extends AbstractException {

    public EmptyIdException() {
        super("Error. Id is empty");
    }

    public EmptyIdException(String value) {
        super("Error. " + value + " id is empty");
    }

}
