package ru.korkmasov.tsc.api.service;

import org.jetbrains.annotations.NotNull;
import ru.korkmasov.tsc.api.IPropertyService;
import ru.korkmasov.tsc.api.service.dto.*;

public interface ServiceLocator {

    @NotNull
    ITaskRecordService getTaskRecordService();

    @NotNull
    IProjectRecordService getProjectRecordService();

    @NotNull
    IProjectTaskRecordService getProjectTaskRecordService();

    @NotNull
    IUserRecordService getUserRecordService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ISessionRecordService getSessionRecordService();

    @NotNull
    IDataService getDataService();

    @NotNull
    IConnectionService getConnectionService();
}
