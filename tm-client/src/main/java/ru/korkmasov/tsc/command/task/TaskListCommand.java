package ru.korkmasov.tsc.command.task;

import ru.korkmasov.tsc.endpoint.Task;
import ru.korkmasov.tsc.enumerated.Sort;
import ru.korkmasov.tsc.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class TaskListCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-list";
    }

    @NotNull
    @Override
    public String description() {
        return "Show task list";
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        System.out.printf("\t| %-36s | %-12s | %-20s | %-30s | %-30s | %-30s | %s %n", "ID", "STATUS", "NAME", "CREATED", "STARTED", "FINISHED", "PROJECT");
        final List<Task> list = serviceLocator.getTaskEndpoint().findTaskAll(serviceLocator.getSession());
        if (list == null) return;
        @NotNull AtomicInteger index = new AtomicInteger(1);
        //list.forEach((x) -> System.out.printf("%-3s | %-36s | %-12s | %-20s | %-30s | %-30s | %-30s | %s %n", index.getAndIncrement(), x.getId(), x.getStatus(), x.getName(), x.getCreated(), x.getDateStart(), x.getDateFinish(), x.getProjectId()));
    }

}
