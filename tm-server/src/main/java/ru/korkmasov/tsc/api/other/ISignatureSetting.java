package ru.korkmasov.tsc.api.other;

import org.jetbrains.annotations.NotNull;

public interface ISignatureSetting {

    @NotNull
    String getSignatureSecret();

    @NotNull
    Integer getSignatureIteration();

}
