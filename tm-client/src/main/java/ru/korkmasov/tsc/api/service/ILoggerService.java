package ru.korkmasov.tsc.api.service;

import org.jetbrains.annotations.Nullable;

public interface ILoggerService {

    void info(@Nullable String message);

    void debug(String message);

    void command(@Nullable String message);

    void error(@Nullable Exception e);

}
