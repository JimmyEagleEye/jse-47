package ru.korkmasov.tsc.command.project;

import ru.korkmasov.tsc.endpoint.Project;
import ru.korkmasov.tsc.exception.empty.EmptyNameException;
import ru.korkmasov.tsc.exception.entity.ProjectNotFoundException;
import ru.korkmasov.tsc.util.TerminalUtil;
import ru.korkmasov.tsc.util.ValidationUtil;

import org.jetbrains.annotations.Nullable;
import static ru.korkmasov.tsc.util.TerminalUtil.incorrectValue;

public class ProjectByIdUpdateCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-update-by-id";
    }

    @Override
    public String description() {
        return "Update project by id";
    }

    @Override
    public void execute() {
        System.out.println("Enter id");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final Project project = serviceLocator.getProjectEndpoint().findProjectById(serviceLocator.getSession(), id);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Enter name");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        @Nullable final String description = TerminalUtil.nextLine();
        @Nullable final Project projectUpdated = serviceLocator.getProjectEndpoint().updateProjectById(serviceLocator.getSession(), id, name, description);
        if (projectUpdated == null) incorrectValue();

    }

}

