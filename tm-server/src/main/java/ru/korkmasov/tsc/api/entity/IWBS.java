package ru.korkmasov.tsc.api.entity;

public interface IWBS extends IHasStartDate, IHasName, IHasCreated, IHasStatus {
}