package ru.korkmasov.tsc.exception.empty;


import ru.korkmasov.tsc.exception.AbstractException;

public class EmptyNameException extends AbstractException {

    public EmptyNameException() {
        super("Error. Name is empty");
    }

}
