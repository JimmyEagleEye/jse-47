package ru.korkmasov.tsc.command.system;

import ru.korkmasov.tsc.command.AbstractCommand;

import org.jetbrains.annotations.NotNull;

public final class HelpCommand extends AbstractCommand {

    @Override
    public @NotNull
    final String arg() {
        return "-h";
    }

    @Override
    public @NotNull
    final String name() {
        return "help";
    }

    @Override
    public @NotNull
    final String description() {
        return "Show all commands";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        for (final AbstractCommand command : serviceLocator.getCommandService().getCommands()) {
            System.out.println(command.name() + ": " + command.description());
        }
    }
}